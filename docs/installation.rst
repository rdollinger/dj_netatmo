============
Installation
============

At the command line::

    $ easy_install dj_netatmo

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv dj_netatmo
    $ pip install dj_netatmo
