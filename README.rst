=============================
dj_netatmo
=============================

.. image:: https://badge.fury.io/py/dj_netatmo.png
    :target: https://badge.fury.io/py/dj_netatmo

.. image:: https://travis-ci.org/yourname/dj_netatmo.png?branch=master
    :target: https://travis-ci.org/yourname/dj_netatmo

Manage Netatmo Thermostat

Documentation
-------------

The full documentation is at https://dj_netatmo.readthedocs.org.

Quickstart
----------

Install dj_netatmo::

    pip install dj_netatmo

Then use it in a project::

    import dj_netatmo

Features
--------

* TODO

Running Tests
--------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install -r requirements_test.txt
    (myenv) $ python runtests.py

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
